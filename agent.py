import tensorflow as tf
import random
import numpy as np
import skvideo.io
# from skimage import io
import signal
from pathlib import Path
from tqdm import tqdm

from dueling_dqn import DuelingDQN
from history import History
from atarienvironment import AtariEnvironment
from per_memory import PERMemory

random_seed = 123
tf.set_random_seed(random_seed)
random.seed(random_seed)

# class to get kill signal
class GracefulKiller:
  kill_now = False
  def __init__(self):
    signal.signal(signal.SIGINT, self.exit_gracefully)
    signal.signal(signal.SIGTERM, self.exit_gracefully)

  def exit_gracefully(self,signum, frame):
    self.kill_now = True

class Agent():
    def __init__(self, flags):
        self.flags = flags
        self.env = AtariEnvironment(self.flags)
        self.env_test = AtariEnvironment(self.flags, is_training=False)
        self.flags.action_size = self.env.get_action_size()
        self.hist = History(self.flags)
        self.sess = tf.Session()

        # initialize dqn network
        self.main_dqn = DuelingDQN(self.flags, self.sess, 'main_dqn')
        self.target_dqn = DuelingDQN(self.flags, self.sess, 'target_dqn')

        # initialize replay memory
        self.memory = PERMemory(self.flags)

        self.killer = GracefulKiller()

    def create_copy_op(self, src_weights, target_weights, name):
        with tf.variable_scope(name):
            op_holder = []
            for src, target in zip(src_weights, target_weights):
                op_holder.append(target.assign(src.value()))
            return op_holder

    def variable_summary(self, vars):
        for var in vars:
            tf.summary.histogram('histogram_'+var.name, var)

    def get_onestep_y(self, next_state, reward, terminal):
        # get q values of next state from target dqn
        target_next_q = self.sess.run(self.target_dqn.q, feed_dict={self.target_dqn.states: next_state})

        # get actions from main_dqn which maximize q value of next state (double dqn)
        action_from_main = self.sess.run(self.main_dqn.action_from_q, 
                                         feed_dict={self.main_dqn.states: next_state})

        # get maximu q values from main_dqn actions
        max_target_next_q = target_next_q[range(self.flags.batch_size), action_from_main]

        # get y value
        y_onestep = (1-terminal)*self.flags.discount_factor*max_target_next_q + reward

        return y_onestep, max_target_next_q

    def get_nstep_y(self, indexes, max_target_next_q, terminal):
        # get R from double q learning
        R = (1-terminal)*max_target_next_q

        y_nstep = []
        # foward view n-step
        for r, idx in zip(R, indexes):
            reward = r
            for i in range(1,self.flags.return_steps+1):
                cur_index = idx - i
                if cur_index<0 or self.memory.terminals[cur_index]:
                    break
                reward = self.memory.rewards[cur_index] + self.flags.discount_factor*reward
            y_nstep.append(reward)

        return y_nstep

    # do reward clipping accoreding to DQfD paper
    def get_clipped_reward(self, reward):
        return np.sign(reward)*np.log(1.+abs(reward))

    def train(self):
        # create copy operation
        copy_op = self.create_copy_op(self.main_dqn.weights, self.target_dqn.weights, 'copy_to_trarget_dqn')

        # create train operation
        train_op = self.main_dqn.create_train_op()

        # Set tf variable for saving step index
        tf_step = tf.Variable(0, trainable=False, dtype=tf.int32, name='step')

        # summary
        self.file_writer = tf.summary.FileWriter(self.flags.logdir, self.sess.graph)
        self.variable_summary(self.main_dqn.weights)
        merged = tf.summary.merge_all()

        q_learning_losses = []
        n_step_losses = []
        supervised_losses = []
        reg_losses = []
        total_losses = []

        # add saver and check whether to restore
        saver = tf.train.Saver()
        checkpoint_path = tf.train.latest_checkpoint(self.flags.logdir)
        if checkpoint_path is not None:
            saver.restore(self.sess, checkpoint_path)
            start_step = self.sess.run(tf_step)
            do_pre_training = False
        else:
            start_step = 0
            self.sess.run(tf.global_variables_initializer())
            # copy main_dqn to target_dqn
            self.sess.run(copy_op)
            do_pre_training = True

        if do_pre_training:
            print('Start pre-training')

            # load demonstration data
            demon_path = Path(self.flags.demon_path)
            file_names = list(demon_path.glob('*.npy'))
            for i,name in enumerate(file_names):
                print('Load', name)
                data = np.load(name)
                if i==0:
                    images = data.item().get('images')
                    rewards = data.item().get('rewards')
                    terminals = data.item().get('terminals')
                    actions = data.item().get('actions')
                else:
                    images += data.item().get('images')
                    rewards += data.item().get('rewards')
                    terminals += data.item().get('terminals')
                    actions += data.item().get('actions')

            # add demonstration data to self.memory
            for obs, reward, terminal, action in zip(images, rewards, terminals, actions):
                if self.flags.env_name=='BreakoutDeterministic-v4' and reward>0.0:
                    reward = 1.0
                else:
                    reward = max(self.flags.min_reward, min(self.flags.max_reward, reward))
                self.memory.add(obs, reward, terminal, action)

            print('Nummber Transitions: ', self.memory.count)

            # pre-training
            # for step in range(self.flags.pre_training_step):
            for step in tqdm(range(self.flags.pre_training_step)):
                # sample training data from replay memory
                beta = self.flags.beta + (1.0-self.flags.beta)/self.flags.pre_training_step*step
                data, indexes, weight = self.memory.sample(beta)
                state, reward, terminal, action, next_state = data

                # get one step y values
                y_onestep, max_target_next_q = self.get_onestep_y(next_state, reward, terminal)

                # get n-step y values
                y_nstep = self.get_nstep_y(indexes, max_target_next_q, terminal)
                
                # get supervised flat
                supervised_flag = self.memory.get_supervised_flag(indexes)

                # run train_op and get td_error and total loss
                inputs = [train_op, self.main_dqn.q_learning_loss, self.main_dqn.n_step_loss, self.main_dqn.reg_loss, 
                          self.main_dqn.supervised_loss, self.main_dqn.loss, self.main_dqn.td_error]
                results = self.sess.run(inputs, feed_dict={self.main_dqn.y_onestep: y_onestep,
                                                           self.main_dqn.y_nstep: y_nstep, 
                                                           self.main_dqn.supervised_flag: supervised_flag,
                                                           self.main_dqn.states: state,
                                                           self.main_dqn.action: action, 
                                                           self.main_dqn.is_weights: weight})
                q_learning_losses.append(np.mean(results[1]))
                n_step_losses.append(np.mean(results[2]))
                reg_losses.append(results[3])
                supervised_losses.append(np.mean(results[4]))
                total_losses.append(results[5])
                td_error = results[6]

                # update priori
                self.memory.update_priori(indexes, td_error)

                if step!=0 and step%self.flags.target_update_freq == 0:
                    self.sess.run(copy_op)

                if (step+1)%self.flags.log_every_n_step == 0:
                    # Save summaries
                    total_loss = np.mean(total_losses)
                    summary = tf.Summary(value=[
                        tf.Summary.Value(
                            tag='pre-training/per_beta', simple_value=float(beta)),
                        tf.Summary.Value(
                            tag='pre-training/loss_q_learning', simple_value=float(np.mean(q_learning_losses))),
                        tf.Summary.Value(
                            tag='pre-training/loss_n_step', simple_value=float(np.mean(n_step_losses))),
                        tf.Summary.Value(
                            tag='pre-training/loss_supervised', simple_value=float(np.mean(supervised_losses))),
                        tf.Summary.Value(
                            tag='pre-training/loss_regularization', simple_value=float(np.mean(reg_losses))),
                        tf.Summary.Value(
                            tag='pre-training/total_loss', simple_value=float(total_loss)),
                    ])
                    self.file_writer.add_summary(summary, step)

                    summary = self.sess.run(merged)
                    self.file_writer.add_summary(summary, step)

                    print('\nstep: {}, avg_loss: {:.6f}'.format(step, total_loss))

            # Save model
            self.sess.run(tf.assign(tf_step, step))
            saver.save(self.sess, self.flags.check_point_file)

            # save per_memory and sum_tree
            print('\nSaving pre-training memory data ...')
            path = Path(self.flags.memory_path)
            if not path.exists():
                path.mkdir(parents=True)
            self.memory.save(path)

        print('pre-training is completed')

        # summary
        self.file_writer = tf.summary.FileWriter(self.flags.logdir+'training/', self.sess.graph)

        # add saver and check whether to restore
        checkpoint_path = tf.train.latest_checkpoint(self.flags.logdir+'training/')
        if checkpoint_path is not None:
            saver.restore(self.sess, checkpoint_path)
            start_step = self.sess.run(tf_step)
            print('restored from step: ', start_step)
            # load memory data
            print('Load memory ...')
            self.memory.load(Path(self.flags.memory_path+'/training'))
        else:
            start_step = 0
            self.sess.run(tf.global_variables_initializer())
            # copy main_dqn to target_dqn
            self.sess.run(copy_op)
            # print('Load pre-training memory ...')
            # self.memory.load(Path(self.flags.memory_path))
            self.memory.set_training()

        reward_sum = 0
        num_episode = 0
        rewards = []
        q_learning_losses = []
        n_step_losses = []
        supervised_losses = []
        reg_losses = []
        total_losses = []

        do_eval = False
        do_save_memory = False
        rewards_test = []

        # start a new game
        obs, _, _ = self.env.new_game()
        for _ in range(self.flags.history_length):
            self.hist.add(obs)
        
        # training
        print('start training')
        # for step in range(start_step, self.flags.max_train_step):
        for step in tqdm(range(start_step, self.flags.max_train_step)):
            # calculate ep
            cur_ep = self.flags.ep_start - step*(self.flags.ep_start-self.flags.ep_end)/self.flags.ep_end_frame
            cur_ep = max(self.flags.ep_end, cur_ep)

            # take action from e-greedy
            cur_state = self.hist.get()
            if random.random() <= cur_ep:
                action = random.randrange(self.flags.action_size)
            else:
                action = self.main_dqn.action_eval(cur_state)

            # step action
            obs, reward, done = self.env.step(action)
            self.hist.add(obs)
            
            # reward clipping
            reward = max(self.flags.min_reward, min(self.flags.max_reward, reward))
            reward_sum += reward

            # store transition to replay memory
            self.memory.add(obs, reward, done, action)

            if done:
                # start a new game
                obs, _, _ = self.env.new_game()
                for _ in range(self.flags.history_length):
                    self.hist.add(obs)

                rewards.append(reward_sum)
                reward_sum = 0
                num_episode += 1

                if num_episode%self.flags.eval_every_n_episode == 0:
                    do_eval = True

            if step%self.flags.train_freq == 0:
                beta = self.flags.beta + (1.0-self.flags.beta)/self.flags.max_train_step*step
                data, indexes, weight = self.memory.sample(beta)
                state, reward, terminal, action, next_state = data
                
                # get one step y values
                y_onestep, max_target_next_q = self.get_onestep_y(next_state, reward, terminal)

                # get n-step y values
                y_nstep = self.get_nstep_y(indexes, max_target_next_q, terminal)

                # get supervised flag
                supervised_flag = self.memory.get_supervised_flag(indexes)

                # run train_op and get td_error and total loss
                inputs = [train_op, self.main_dqn.q_learning_loss, self.main_dqn.n_step_loss, self.main_dqn.reg_loss, 
                          self.main_dqn.supervised_loss, self.main_dqn.loss, self.main_dqn.td_error]
                results = self.sess.run(inputs, feed_dict={self.main_dqn.y_onestep: y_onestep,
                                                           self.main_dqn.y_nstep: y_nstep, 
                                                           self.main_dqn.supervised_flag: supervised_flag,
                                                           self.main_dqn.states: state,
                                                           self.main_dqn.action: action, 
                                                           self.main_dqn.is_weights: weight})
                q_learning_losses.append(np.mean(results[1]))
                n_step_losses.append(np.mean(results[2]))
                reg_losses.append(results[3])
                supervised_losses.append(np.mean(results[4]))
                total_losses.append(results[5])
                td_error = results[6]

                # update priori
                self.memory.update_priori(indexes, td_error)

            if step!=0 and step%self.flags.target_update_freq == 0:
                self.sess.run(copy_op)

            # write summary 
            if num_episode%self.flags.log_every_n_episode == 0:
                if num_episode==0:
                    continue

                # Save summaries
                avg_reward = np.mean(rewards)
                max_reward = np.max(rewards)
                min_reward = np.min(rewards)
                total_loss = np.mean(total_losses)
                summary = tf.Summary(value=[
                    tf.Summary.Value(
                        tag='train/avg_reward', simple_value=float(avg_reward)),
                    tf.Summary.Value(
                        tag='train/max_reward', simple_value=float(max_reward)),
                    tf.Summary.Value(
                        tag='train/min_reward', simple_value=float(min_reward)),
                    tf.Summary.Value(
                        tag='train/epsilon', simple_value=float(cur_ep)),
                    tf.Summary.Value(
                        tag='train/per_beta', simple_value=float(beta)),
                    tf.Summary.Value(
                        tag='train/loss_q_learning', simple_value=float(np.mean(q_learning_losses))),
                    tf.Summary.Value(
                        tag='train/loss_n_step', simple_value=float(np.mean(n_step_losses))),
                    tf.Summary.Value(
                        tag='train/loss_supervised', simple_value=float(np.mean(supervised_losses))),
                    tf.Summary.Value(
                        tag='train/loss_regularization', simple_value=float(np.mean(reg_losses))),
                    tf.Summary.Value(
                        tag='train/total_loss', simple_value=float(total_loss)),
                ])
                self.file_writer.add_summary(summary, step)

                summary = self.sess.run(merged)
                self.file_writer.add_summary(summary, step)

                print('\n=> Training Result')
                print('step: {}, loss: {:.6f}'.format(step, total_loss))
                print('avg reward: {:.6f}, min reward: {:.6f}, max reward: {:.6f}'.format(avg_reward, min_reward, max_reward))
                print('beta: {:.6f}, max_priori: {:.6f}, min_priori: {:.6f}'.format(
                                beta, self.memory.max_priori, self.memory.min_priori))

                rewards = []
                num_episode = 0
                q_learning_losses = []
                n_step_losses = []
                supervised_losses = []
                reg_losses = []
                total_losses = []

            # do evaluation
            if do_eval:
                do_eval = False
                rewards_test.append(self.eval())

                if len(rewards_test)==self.flags.log_every_n_eval:
                    avg_reward = np.mean(rewards_test)
                    max_reward = np.max(rewards_test)
                    min_reward = np.min(rewards_test)

                    # Save evaluation summaries
                    summary = tf.Summary(value=[
                        tf.Summary.Value(
                            tag='eval/avg_reward', simple_value=float(avg_reward)),
                        tf.Summary.Value(
                            tag='eval/max_reward', simple_value=float(max_reward)),
                        tf.Summary.Value(
                            tag='eval/min_reward', simple_value=float(min_reward)),
                    ])
                    self.file_writer.add_summary(summary, step)

                    print('\n=> Evaluation result at step: ', step)
                    print('avg reward: {:.6f}, min reward: {:.6f}, max reward: {:.6f}'.format(avg_reward, min_reward, max_reward))

                    rewards_test = []

            # save model and memory every self.flags.save_every_n_step steps
            if step!=0 and step%self.flags.save_every_n_step==0:
                do_save_memory = True

            # when application gets kill signal and terminal state or every at 1,000,000 steps
            if done and (self.killer.kill_now or do_save_memory):
                do_save_memory = False

                # Save model
                self.sess.run(tf.assign(tf_step, step))
                saver.save(self.sess, self.flags.logdir+'training/model.ckpt')

                # save per_memory and sum_tree
                print('\nSaving memory data ...')
                path = Path(self.flags.memory_path+'/training')
                if not path.exists():
                    path.mkdir(parents=True)
                self.memory.save(path)

                if self.killer.kill_now:
                    break

    # evaluation atari game with is_training=False option
    def eval(self):
        hist = History(self.flags)
        reward_sum = 0

        # start a new game
        obs, _, _ = self.env_test.new_game()
        for _ in range(self.flags.history_length):
            hist.add(obs)

        while True:
            # take action from e-greedy
            if random.random() <= self.flags.ep_test:
                action = random.randrange(self.flags.action_size)
            else:
                action = self.main_dqn.action_eval(hist.get())

            # step action
            obs, reward, terminal = self.env_test.step(action)
            hist.add(obs)
            reward_sum += reward

            if terminal:
                break

        return reward_sum

    def test(self):
        tf_step = tf.Variable(0, trainable=False, dtype=tf.int32, name='step')
        
        # add saver and check whether to restore
        saver = tf.train.Saver()
        checkpoint_path = tf.train.latest_checkpoint(self.flags.logdir)
        if checkpoint_path is not None:
            saver.restore(self.sess, checkpoint_path)
            step = self.sess.run(tf_step)
            print('restored from step: ', step)
        else:
            print('None path')
            return

        num_game = 50
        best_reward = 0
        best_frames = []

        env = AtariEnvironment(self.flags, is_training=False)

        for i in range(num_game):
            # start a new game
            obs, _, _ = env.new_game()
            for _ in range(self.flags.history_length):
                self.hist.add(obs)

            reward_sum = 0

            while True:
                # take action from e-greedy
                if random.random() <= self.flags.ep_test:
                    action = random.randrange(self.flags.action_size)
                else:
                    action = self.main_dqn.action_eval(self.hist.get())

                # step action
                obs, reward, terminal = env.step(action)
                self.hist.add(obs)
                reward_sum += reward

                if terminal:
                    break

            if reward_sum > best_reward:
                best_reward = reward_sum
                best_frames = env.get_video_frames()

            print('episode: {}, reward: {}, best reward: {}'.format(i, int(reward_sum), int(best_reward)))

        if self.flags.save_video:
            skvideo.io.vwrite('result_'+str(int(best_reward))+'.mp4', best_frames)
            # skvideo.io.vwrite('result_step'+str(int(max_step))+'.mp4', max_step_frames)
            # skvideo.io.vwrite('result_min_'+str(int(min_reward))+'.mp4', min_frames)
