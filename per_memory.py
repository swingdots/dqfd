import numpy as np
import random
import math

from sum_tree import SumTree

class PERMemory:
    def __init__(self, flags):
        self.flags = flags
        self.imsize = [self.flags.input_dim, self.flags.input_dim]
        self.actions = np.empty(self.flags.memory_size, dtype=np.uint8)
        self.rewards = np.empty(self.flags.memory_size, dtype=np.float32)
        self.images = np.empty([self.flags.memory_size]+self.imsize, dtype=np.uint8)
        self.terminals = np.empty(self.flags.memory_size, dtype=np.bool)

        self.sum_tree = SumTree(self.flags.memory_size)
        self.count = 0          # size of replay memory
        self.current = 0        # current index of buffer

        # holder for sampling of state and next state
        self.state = np.empty([self.flags.batch_size,self.flags.history_length]+self.imsize, dtype=np.float32)
        self.next_state = np.empty([self.flags.batch_size,self.flags.history_length]+self.imsize, dtype=np.float32)

        # set segment boundary
        self.seg_bound = [1.0/self.flags.batch_size*i for i in range(self.flags.batch_size+1)]

        # set min/max priori
        self.max_priori = self.flags.max_priori ** self.flags.alpha
        self.min_priori = self.max_priori

        self.is_pretraining = True
        self.begin_of_generated_data_idx = 0

    def set_training(self):
        self.is_pretraining = False
        self.begin_of_generated_data_idx = self.current

    def get_supervised_flag(self, indexes):
        if self.is_pretraining:
            return [1. for _ in range(len(indexes))]
        else:
            # return [idx<self.begin_of_generated_data_idx for idx in indexes]
            return [1. if idx<self.begin_of_generated_data_idx else 0. for idx in indexes]

    # add transition data
    def add(self, obs, reward, terminal, action):
        # add transition data
        self.images[self.current, ...] = obs
        self.rewards[self.current] = reward
        self.terminals[self.current] = terminal
        self.actions[self.current] = action

        # update priori with max_priori
        self.sum_tree.update_priori(self.current, self.max_priori)

        # assign current count and index
        self.count = max(self.count, self.current + 1)
        self.current = (self.current + 1) % self.flags.memory_size
        if self.current==0:
            self.current = self.begin_of_generated_data_idx

    def sample_index(self):
        indexes = []
        priorities = []
        index_count = 0

        while True:
            # sample random float value between segment
            val = random.uniform(self.seg_bound[index_count], self.seg_bound[index_count+1])
            # get pioriti and index corresponding to val
            index, priori = self.sum_tree.find(val)

            if index>=self.count:
                continue

            # to stack (self.flags.history_length) images skip the first (self.flags.history_length) images.
            # set to zero not to be selected
            if index-self.flags.history_length < 0:
                self.sum_tree.update_priori(index, 0.0)
                self.update_min_max_priori(priori)
                continue

            # if there's true in self.terminals of cur_state index, skip index.
            # set to zero not to be selected
            if self.terminals[index-self.flags.history_length:index].any():
                self.sum_tree.update_priori(index, 0.0)
                self.update_min_max_priori(priori)
                continue

            indexes.append(index)
            priorities.append(priori)
            index_count += 1
            if index_count==self.flags.batch_size:
                break
        
        return indexes, priorities

    # sample transition data and weight
    def sample(self, beta):
        # get index and priori
        indexes, priorities = self.sample_index()

        # assign reward, terminal and action batch
        reward = self.rewards[indexes]
        terminal = self.terminals[indexes]
        action = self.actions[indexes]

        # stack images to get current/next state
        for i, idx in enumerate(indexes):
            self.state[i, ...] = self.images[idx-4:idx, ...]
            self.next_state[i, ...] = self.images[idx-3:idx+1, ...]

        # normalize images between 0.0 and 1.0
        self.state /= 255
        self.next_state /= 255
        # cast to float32 and transpose to [batch_size, input_dim, input_dim, history_length]
        state = np.transpose(self.state, [0,2,3,1]).astype(np.float32)
        next_state = np.transpose(self.next_state, [0,2,3,1]).astype(np.float32)

        # calculate weight
        priorities = np.asarray(priorities)
        Pi = priorities / self.sum_tree.get_priori_sum()
        weight = (self.flags.memory_size*Pi) ** (-beta)
        weight /= np.max(weight)

        return (state, reward, terminal, action, next_state), indexes, weight

    # update tree value
    def update_priori(self, indexes, td_error):
        # calculate prior value
        if self.is_pretraining:
            priori = (td_error + self.flags.per_ep_demon) ** self.flags.alpha
        else:
            replay_constants = np.ones_like(indexes)*self.flags.per_ep
            replay_constants[np.asarray(indexes)<self.begin_of_generated_data_idx] = self.flags.per_ep_demon
            priori = (td_error + replay_constants) ** self.flags.alpha
        
        for idx, pri in zip(indexes, priori):
            self.sum_tree.update_priori(idx, pri)
            self.max_priori = max(self.max_priori, pri)
            self.min_priori = min(self.min_priori, pri)

    # adjust min, max priori
    def update_min_max_priori(self, priori):
        if priori == self.max_priori:
            self.max_priori = self.sum_tree.get_max_priori()
        if priori == self.min_priori:
            self.min_priori = self.sum_tree.get_min_priori()

    # save per_memory variables
    def save(self, path):
        self.sum_tree.save(path)

        save_data = {}
        save_data['actions'] = self.actions
        save_data['rewards'] = self.rewards
        save_data['terminals'] = self.terminals
        save_data['count'] = self.count
        save_data['current'] = self.current
        save_data['is_pretraining'] = self.is_pretraining
        save_data['begin_of_generated_data_idx'] = self.begin_of_generated_data_idx

        data_path = path / 'transition_data.npy'
        np.save(data_path, save_data)
        image_path = path / 'images.npy'
        np.save(image_path, self.images)

    # load per_memory variables
    def load(self, path):
        self.sum_tree.load(path)

        data_path = path / 'transition_data.npy'
        data = np.load(data_path)
        self.actions = data.item().get('actions')
        self.rewards = data.item().get('rewards')
        self.terminals = data.item().get('terminals')
        self.count = data.item().get('count')
        self.current = data.item().get('current')
        self.is_pretraining = data.item().get('is_pretraining')
        self.begin_of_generated_data_idx = data.item().get('begin_of_generated_data_idx')
        
        image_path = path / 'images.npy'
        self.images = np.load(image_path)

        self.max_priori = self.sum_tree.get_max_priori()
        self.min_priori = self.sum_tree.get_min_priori()
