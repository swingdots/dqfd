import tensorflow as tf
import argparse

from agent import Agent

def parse_args():
    parser = argparse.ArgumentParser()

    # Saver parameters
    parser.add_argument('--log_path', type=str, default='../../train/reinforcement/dqfd/', 
                                      help='path to log directory')
    parser.add_argument('--log_every_n_step', type=int, default=1000,
                                              help='log pre-train every n step')
    parser.add_argument('--log_every_n_episode', type=int, default=100,
                                                 help='log every n episode')
    parser.add_argument('--eval_every_n_episode', type=int, default=10,
                                                  help='evaluate every n episode')
    parser.add_argument('--log_every_n_eval', type=int, default=20,
                                              help='log evaluation every n eval')
    parser.add_argument('--save_every_n_step', type=int, default=1000000,
                                               help='save model every n step')
    parser.add_argument('--memory_path', type=str, default='./data',
                                         help='path for saving memory data')
    parser.add_argument('--demon_path', type=str, default='../../data/dqfd/demon_data/01', 
                                        help='path for demonstration data')

    # DQfD parameters
    parser.add_argument('--pre_training_step', type=int, default=750000, 
                                               help='max pre-training steps')
    # parser.add_argument('--pre_training_step', type=int, default=100000,
    #                     help='max pre-training steps')
    parser.add_argument('--n_step_weight', type=float, default=1.0, 
                                           help='N-Step Return weight')
    parser.add_argument('--supervised_weight', type=float, default=1.0, 
                                               help='Supervised loss weight')
    parser.add_argument('--reg_weight', type=float, default=1e-5, 
                                        help='L2 regularization weight')
    parser.add_argument('--expert_margin', type=float, default=0.8, 
                                           help='Expert margin for supervised loss')
    parser.add_argument('--return_steps', type=int, default=10, 
                                          help='N-step returns with return_steps')

    # Perioritized replay memory parameters
    parser.add_argument('--max_priori', type=float, default=1.0, 
                                        help='maximum')
    parser.add_argument('--alpha', type=float, default=0.6, 
                                   help='')
    parser.add_argument('--beta', type=float, default=0.4, 
                                  help='')
    parser.add_argument('--per_ep', type=float, default=0.001, 
                                    help='')
    parser.add_argument('--per_ep_demon', type=float, default=1.0,
                                          help='')

    # Model parameters
    parser.add_argument('--input_dim', type=int, default=84, 
                                       help='The size of input image(width and height are equal)')
    parser.add_argument('--batch_size', type=int, default=32, 
                                        help='number of training cases')
    # parser.add_argument('--batch_size', type=int, default=5,
    #                     help='number of training cases')
    parser.add_argument('--memory_size', type=int, default=1000000,
                                         help='The size of experience memory')
    parser.add_argument('--history_length', type=int, default=4,
                                            help='The length of history of observation to use as an input to DQN')
    parser.add_argument('--target_update_freq', type=int, default=10000,
                                                help='the frequency with which the target network is update')
    parser.add_argument('--train_freq', type=int, default=4,
                                        help='the number of actions between successive SGD updates')
    parser.add_argument('--discount_factor', type=float, default=0.99,
                                             help='The discount factor for reward')
    parser.add_argument('--learning_rate', type=float, default=0.00025,
                                           help='learning rate')
    parser.add_argument('--momentum', type=float, default=0.95,
                                      help='momentum used by RMSProp')
    parser.add_argument('--epsilon', type=float, default=0.01,
                                     help='constant added to the RMSProp to avoid zero denominator')
    parser.add_argument('--ep_start', type=float, default=1.0,
                                      help='The value of epsilon at start in e-greedy')
    parser.add_argument('--ep_end', type=float, default=0.01,
                                    help='The value of epsilnon at the end in e-greedy')
    parser.add_argument('--ep_test', type=float, default=0.001,
                                     help='The value of epsilnon when test')
    parser.add_argument('--ep_end_frame', type=int, default=1000000,
                                          help='The value of step at the end in e-greedy')
    parser.add_argument('--no_op_max', type=int, default=7,
                                       help='maximum number of "do nothing" actions')
    parser.add_argument('--max_train_step', type=int, default=50000000,
                                            help='The maximum step while training')
    # parser.add_argument('--max_train_step', type=int, default=20000000,
    #                     help='The maximum step while training')
    parser.add_argument('--max_reward', type=float, default=1.0, 
                                        help='The maximum value of clipped reward')
    parser.add_argument('--min_reward', type=float, default=-1.0, 
                                        help='The minimum value of clipped reward')
    parser.add_argument('--use_clipping', type=bool, default=False, 
                                          help='use gradient clipping')
    parser.add_argument('--grad_clip', type=float, default=10.0,
                                       help='max norm of global gradient clipping')
    # parser.add_argument('--use_dueling_dqn', type=bool, default=False,
    #                                           help='use dueling dqn')

    # etc parameters
    parser.add_argument('--env_name', type=str, default='BreakoutDeterministic-v4', 
                                      help='name of atari environment')
    parser.add_argument('--is_training', type=bool, default=True, 
                                         help='')
    parser.add_argument('--mode', type=str, default='train', 
                                  help='train/test')
    parser.add_argument('--is_display', type=bool, default=False, 
                                        help='')
    parser.add_argument('--save_video', type=bool, default=True, 
                                        help='')

    return modify_dir_name(parser.parse_args())

def modify_dir_name(args):
    # set default directory name
    args.dir_name = args.env_name
    
    args.dir_name += '/dueling_double_dqn'

    # gradient clipping
    if args.use_clipping:
        args.dir_name += '/use_clipping'
    if args.grad_clip != 10.0:
        args.dir_name += '/grad_clip_' + str(args.grad_clip)
    # learning rate
    if args.learning_rate != 0.00025:
        args.dir_name += '/lr_' + str(args.learning_rate)

    # args.dir_name += '/nstep_test/reward_clipping'

    # set log directory 
    args.logdir = args.log_path + args.dir_name + '/'
    args.check_point_file = args.logdir + 'model.ckpt'

    # set data directory
    args.memory_path += '/' + args.dir_name

    return args
    
"""main function"""
def main(args):
    # Print all arguments
    for key, val in vars(args).items():
        print(key, ":", val)

    agent = Agent(args)

    if args.mode == 'train':
        agent.train()
    elif args.mode == 'test':
        agent.test()
    else:
        print('wrong mode')

if __name__ == '__main__':
    args = parse_args()

    main(args)