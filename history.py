import numpy as np
from collections import deque

class History:
    def __init__(self, flags):
        self.flags = flags
        self.dims = [flags.input_dim, flags.input_dim, self.flags.history_length]
        self.buffer = np.zeros(self.dims, dtype=np.float32)

    def add(self, obs):
        self.buffer[:,:,:-1] = self.buffer[:,:,1:]
        # cast to float32 and normalize between 0.0 and 1.0
        self.buffer[:,:,-1] = (obs/255).astype(np.float32)

    def get(self):
        return self.buffer
