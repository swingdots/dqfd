import tensorflow as tf
import numpy as np

from atarienvironment import AtariEnvironment
from history import History

class DuelingDQN():
    def __init__(self, flags, sess, network_name):
        self.flags = flags
        self.sess = sess
        self.network_name = network_name
        self.build_model()

    def build_model(self):
        self.states =  tf.placeholder('float32',
                                      [None, self.flags.input_dim, self.flags.input_dim, self.flags.history_length],
                                      name='states')

        with tf.variable_scope(self.network_name):
            out = tf.layers.conv2d(inputs=self.states, filters=32,
                                   kernel_size=[8,8], strides=(4,4),
                                   activation=tf.nn.relu, 
                                   name='conv2d_1')
            out = tf.layers.conv2d(out, filters=64, 
                                   kernel_size=[4,4], strides=(2,2), 
                                   activation=tf.nn.relu, 
                                   name='conv2d_2')
            out = tf.layers.conv2d(out, filters=64, 
                                   kernel_size=[3,3], strides=(1, 1),
                                   activation=tf.nn.relu, 
                                   name='conv2d_3')
            out = tf.layers.flatten(out)
            
            # value stream
            fc_val = tf.layers.dense(out, 512, activation=tf.nn.relu, name='fc_val')
            value = tf.layers.dense(fc_val, 1, name='output_val')

            # advantage stream
            fc_adv = tf.layers.dense(out, 512, activation=tf.nn.relu, name='fc_advantage')
            advantage = tf.layers.dense(fc_adv, self.flags.action_size, name='output_adv')

            # get q value from equation 9
            self.q = value + (advantage - tf.reduce_mean(advantage, axis=1, keep_dims=True))

        self.action_from_q = tf.argmax(self.q, axis=1)

        self.weights = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope=self.network_name)

    def q_eval(self, state):
        return self.sess.run(self.q, feed_dict={self.states: [state]})[0]

    def action_eval(self, state):
        return self.sess.run(self.action_from_q, feed_dict={self.states: [state]})[0]

    def create_train_op(self):
        # placeholders
        self.y_onestep = tf.placeholder(dtype=tf.float32, shape=[None], name='y_onestep')
        self.y_nstep = tf.placeholder(dtype=tf.float32, shape=[None], name='y_nstep')

        # action in current state
        self.action = tf.placeholder(dtype=tf.int32, shape=[None], name='action')
        # importance-sampling weights
        self.is_weights = tf.placeholder(dtype=tf.float32, shape=[None], name='is_weight')

        # get q value of action
        action_one_hot = tf.one_hot(self.action, self.flags.action_size)
        q_acted = tf.reduce_sum(self.q * action_one_hot, axis=1)

        # calculate one step q learning loss
        q_delta = self.y_onestep - q_acted
        clipped_q_delta = tf.where(tf.abs(q_delta) < 1.0,
                                   0.5 * tf.square(q_delta),
                                   tf.abs(q_delta) - 0.5)
        self.q_learning_loss = clipped_q_delta

        # calculate n-step q learning loss
        nstep_q_delta = self.y_nstep - q_acted
        clipped_nstep_q_delta = tf.where(tf.abs(nstep_q_delta) < 1.0,
                                         0.5 * tf.square(nstep_q_delta),
                                         tf.abs(nstep_q_delta) - 0.5)
        self.n_step_loss = self.flags.n_step_weight * clipped_nstep_q_delta

        # calculate supervised loss
        self.supervised_flag = tf.placeholder(dtype=tf.float32, shape=[None], name='supervised_flag')
        # set supervised margin
        l_margin = tf.one_hot(self.action, self.flags.action_size, on_value=0., off_value=self.flags.expert_margin)
        loss = tf.reduce_max((self.q + l_margin), axis=1) - q_acted
        self.supervised_loss = tf.square(tf.abs(loss)) * self.flags.supervised_weight * self.supervised_flag

        # calculate L2 regularization loss
        l2_loss = 0.0
        with tf.variable_scope('l2_regularization_loss'):
            for weight in self.weights:
                l2_loss += tf.nn.l2_loss(weight)
        self.reg_loss = self.flags.reg_weight * l2_loss

        # get td error
        self.td_error = tf.abs(q_delta) + tf.abs(nstep_q_delta)

        # calculate total loss
        q_losses = (self.q_learning_loss + self.n_step_loss + self.supervised_loss)*self.is_weights
        self.loss = tf.reduce_mean(q_losses) + self.reg_loss

        optimizer = tf.train.RMSPropOptimizer(learning_rate=self.flags.learning_rate, 
                                              momentum=self.flags.momentum, 
                                              epsilon=self.flags.epsilon)

        # gradient clipping
        if self.flags.use_clipping:
            gradients = tf.gradients(ys=self.loss, xs=self.weights)
            clipped_grads, _ = tf.clip_by_global_norm(gradients, self.flags.grad_clip)

            train_op = optimizer.apply_gradients([(grad, var) for grad, var in zip(clipped_grads, self.weights)])
        else:
            train_op = optimizer.minimize(self.loss)

        return train_op
